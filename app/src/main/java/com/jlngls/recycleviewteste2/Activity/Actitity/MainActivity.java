package com.jlngls.recycleviewteste2.Activity.Actitity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

import com.jlngls.recycleviewteste2.Activity.Adapter.Adapter;
import com.jlngls.recycleviewteste2.Activity.Model.Model;
import com.jlngls.recycleviewteste2.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Model> listaFilme = new ArrayList<>();
    private  ImageView foto;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerViewID);

        Criarfilmes();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        RecyclerView.Adapter adapter = new Adapter(listaFilme);
        recyclerView.setAdapter(adapter);


    }

    public void Criarfilmes() {
        Model filme = new Model("Floresta"," 2020","natereza",null);
        listaFilme.add(filme);

        filme = new Model("Criança"," 2020","masculino",null);
        listaFilme.add(filme);
    }




}
