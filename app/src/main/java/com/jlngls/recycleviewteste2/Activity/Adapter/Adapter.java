package com.jlngls.recycleviewteste2.Activity.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.recycleviewteste2.Activity.Model.Model;
import com.jlngls.recycleviewteste2.Activity.ViewHolder.MyViewHolder;
import com.jlngls.recycleviewteste2.R;

import java.util.List;
import java.util.zip.Inflater;

public class Adapter extends RecyclerView.Adapter<MyViewHolder> {
    private List<Model> filmeLista;

    public Adapter(List<Model> lista) {
        this.filmeLista = lista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.adapter_style, parent, false);

        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Model filme = filmeLista.get(position);
        holder.titulo.setText(filme.getTitulo());
        holder.ano.setText(filme.getAno());
        holder.genero.setText(filme.getGenero());



    }

    @Override
    public int getItemCount() {
        return filmeLista.size();
    }
}
