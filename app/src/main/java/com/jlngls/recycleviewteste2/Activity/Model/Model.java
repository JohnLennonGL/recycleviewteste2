package com.jlngls.recycleviewteste2.Activity.Model;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

public class Model {

    private String titulo;
    private  String ano;
    private  String genero;
    private ImageView imagem;



    public Model(String titulo, String ano, String genero,ImageView imagem) {
        this.titulo = titulo;
        this.ano = ano;
        this.genero = genero;
        this.imagem = imagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public ImageView getImagem() {
        return imagem;
    }

    public void setImagem(ImageView imagem) {
        this.imagem = imagem;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
