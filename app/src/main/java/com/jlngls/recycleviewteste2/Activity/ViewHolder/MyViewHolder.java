package com.jlngls.recycleviewteste2.Activity.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.recycleviewteste2.Activity.Actitity.MainActivity;
import com.jlngls.recycleviewteste2.R;

import java.text.BreakIterator;

public class MyViewHolder extends RecyclerView.ViewHolder {


   public TextView titulo;
    public TextView ano;
   public  TextView genero;
   public ImageView imagem;


    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
         titulo = itemView.findViewById(R.id.tituloID);
         ano = itemView.findViewById(R.id.anoID);
         genero = itemView.findViewById(R.id.generoID);
         imagem = itemView.findViewById(R.id.imagemID);

    }


}

